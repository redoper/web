"""simJOP :: Web

Usage:
  simjop_web run [--port=<port_number>]
  simjop_web (-h | --help)
  simjop_web --version

Options:
  --port=<port_number>  Port [default: 80]
  -h --help             Show this screen.
  --version             Show version.
"""

import os
import logging

from docopt import docopt
import aiohttp_jinja2
import jinja2
from aiohttp import web

from core.routes import setup_routes
from core.views import SiteHandler
from core.utils import mkdir_if_not_exists

STATIC_ROOT = os.path.abspath('./simjop_web/static')
TEMPLATES_ROOT = os.path.abspath('./simjop_web/templates')
LOGS_ROOT = os.path.abspath('./simjop_web/logs')


def setup_jinja(app):
    loader = jinja2.FileSystemLoader(TEMPLATES_ROOT)
    jinja_env = aiohttp_jinja2.setup(app, loader=loader)
    return jinja_env


async def init_app():

    app = web.Application()
    setup_jinja(app)

    handler = SiteHandler()
    setup_routes(app, handler, STATIC_ROOT)
    return app


def main():

    args = docopt(__doc__, version='0.0.1')

    if args['run']:
        mkdir_if_not_exists(LOGS_ROOT)
        log_file = os.path.join(LOGS_ROOT, 'simjop_web.log')
        logging.basicConfig(level=logging.DEBUG, filename=log_file)

        app = init_app()
        web.run_app(app, port=args['--port'])


if __name__ == '__main__':
    main()
